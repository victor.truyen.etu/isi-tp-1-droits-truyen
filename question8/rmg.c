#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "check_pass.h"

int check_grp(char *filename){
    struct stat file_stat;
    if(stat(filename, &file_stat) < 0){
        printf("Error getting file stat\n");
        exit(EXIT_FAILURE);
    }

    int group_id = getgid();
    return group_id == file_stat.st_gid;
}

int main(int argc, char *argv[]){
    char password[40];


    if(argc != 2){
        printf("Usage: %s <filename> as absolute path\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    if (check_grp(argv[1])){
        printf("You are in the group of the file\n");
    }else{
        printf("Access denied, you don't have the rights\n");
        exit(EXIT_FAILURE);
    }
    
    printf("Enter the password: ");
    scanf("%s", &password);

    int uid = getuid();
    if(check_pass(uid,password)){
        int sucess = remove(argv[1]);
        if (sucess == 0){
            printf("File deleted successfully\n");
            return 0;
        }else{
            printf("Error deleting file\n");
            return 1;
        }
    }else{
        return 1;
    }
}