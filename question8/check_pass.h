#ifndef CHECK_PASS_H
#define CHECK_PASS_H

int check_pass(int uid, char *enter_password);
char* parse_password(char *line);
int parse_uid(char *line);

#endif 