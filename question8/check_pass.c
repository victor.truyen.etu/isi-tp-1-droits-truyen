#include "check_pass.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

int check_pass(int enter_uid, char *enter_password)
{
    FILE *fp;
    fp = fopen("/home/myadmin/passwd", "r");
    if (fp == NULL)
    {
        printf("Error opening file\n");
        return 0;
    }else{
        char* line = NULL;
        char *password = NULL;
        int uid = 0;
        size_t len = 0;
        ssize_t read;
        while ((read = getline(&line, &len, fp)) != -1) {
            password = parse_password(line);
            uid = parse_uid(line);
            if (uid == enter_uid && strcmp(password, enter_password) == 0){
                printf("Password is correct\n");
                return 1;
            }
        }
        printf("Password is incorrect\n");
    }
    return 0;
}

char* parse_password(char *line)
{
    char* words = strtok(line, "-");
    char* password = strtok(NULL, "-");
    return password;
}

int parse_uid(char *line)
{
    char* uid = strtok(line, "-");
    return atoi(uid);
}