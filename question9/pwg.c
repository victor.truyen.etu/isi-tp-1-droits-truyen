#include "pwg.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "check_pass.h"

int checkIfExist(int uid){
    FILE *fp;
    fp = fopen("/home/myadmin/passwd", "r");
    if (fp == NULL)
    {
        printf("Error opening file\n");
        return 0;
    }else{
        char* line = NULL;
        char *password = NULL;
        int file_uid = 0;
        int len = 0;
        int read;
        while ((read = getline(&line, &len, fp)) != -1) {
            file_uid = parse_uid(line);
            if (file_uid == uid){
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_FAILURE;
}
int create_new_password(int uid, char* password){
    FILE *fp;
    char* crypt_password;
    fp = fopen("/home/myadmin/passwd", "a");
    crypt_password = crypt(password, "$1$........");
    if (fp == NULL)
    {
        printf("Error opening file\n");
        return 1;
    }else{
        fprintf(fp, "%d-%s\n", uid, password);
        fclose(fp);
        return 0;
    }
}


int main (int argc, char *argv[]){
    int uid = getuid();
    char* password;
    if (checkIfExist(uid) == EXIT_SUCCESS){
        printf("User exists\n");
        scanf("%s", password);
        if (check_pass(uid, password) == EXIT_SUCCESS)
        {
            //replace password
        }
    }else{
        scanf("%s", password);
        return create_new_password(uid, password);
    }
}