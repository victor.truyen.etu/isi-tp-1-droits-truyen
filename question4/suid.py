import os

if __name__ == "__main__":
    print(f'RUID: {os.getuid()}')
    print(f'EUID: {os.geteuid()}')
    print(f'EGID: {os.getegid()}')
    print(f'RGID: {os.getgid()}')
