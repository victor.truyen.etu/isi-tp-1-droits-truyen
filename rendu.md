# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Truyen, Victor, victor.truyen.etu@univ-lille.fr

## Question 1

Ici, le processus lancé par l'utilisateur **toto** n'a le droit d'ouvrir le dit fichier en écriture, car il ne dispose que de la permission *r* soit de la lecture 

## Question 2

Le caractère *x* pour un répertoire veut dire que l'on peut rentrer dedans.
Après avoir crée le répertoire mydir et lui avoir enlevé les permissions pour le groupe *ubuntu*
Si on essaie de rentrer dedans avec l'utilisateur *toto*, on reçois le résultat suivant :

```console
bash: cd: mydir/: Permission denied
```

Quand on essaie après avoir créer le fichier avec l'utilisateur *ubuntu* et que l'on essaie de lister les éléments du répertoire on obtient la réponse suivante :

```console
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
ls: cannot access 'mydir/.': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt
```

Soit on obtient la liste des fichiers présents dans le réppertoire cependant, on a aucune infos  concernant les fichiers que ce soit les permissions ou encore les différents ids
Cela est dut au fait que l'on a au niveau du groupe la permission de lecture mais pas d'execution.

## Question 3

(*voir le code C dans le fichier proposé*)

Quand on essaie d'executer notre propre programme on se retrouve avec ceci :

```console
eiud: 1001
egid: 1001
ruid: 1001
rgid: 1000
Cannot open file: Permission denied
```

On remarque de suite que l'accès en lecture nous est interdit grâce au message d'erreur présent dans notre programme, avec l'utilisateur *toto*. En ce qui concerne les ids, si on fait le parallèle avec la commande `id toto` on obtient ceci :
```console
uid=1001(toto) gid=1000(ubuntu) groups=1000(ubuntu)
```
On remarque donc que les ids sont enfait celui de notre utilisateur *toto* pour la grande majorité d'entre eux.

Lorsque qu'on execute la commande pour set le flag, et que l'on execute notre programme de nouveau avec l'utilisateur *toto*, on obtient ceci :

```console
eiud: 1000
egid: 1000
ruid: 1001
rgid: 1000
File opens correctly
Hello from data.txt

```

On remarque le processus, a bien accès au fichier, et que les valeurs de l'*euid* et de l'*egid* ont changé pour devenir celle  de notre utilisateur *ubuntu*
## Question 4

(*voir le code python dans le fichier proposé*)

Lorsque l'on execute notre script python après avoir fait la modification de privilège, on obtient ceci :

```console
RUID: 1001
EUID: 1001
EGID: 1000
RGID: 1000
```

On remarque imédiatement que les résultats entre notre code C et notre script python sont différents.
On pourrait passer par un fichier dont l'adminstrateur nous a donné ses droits dessus.

## Question 5

La commande **chfn** permet de modifier le nom complet et les informations relatives à un utilisateur.  
Quand on réalise la commande `ls -al /usr/bin/chfn`, on obtient le résultat suivant: 

```console
-rwsr-xr-x 1 root root 72712 Mar 14  2022 /usr/bin/chfn
```

On constate que c'est un binaire exécutable qui dont les permissions ont été réhaussées avec le flag *set-user-id*, il est donc lisible et éxécutable par tout le monde sur le système.

## Question 6

Les mots de passe des utilisateurs sont stockés dans le fichier */etc/shadow*.
Ce fichier est inacessible de tous sauf de l'utilisateur *root*.
De plus les mots de passe à l'intérieur de celui-ci sont hashés.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.
Celui-ci fonctionne avec un fichier de mot de passe avec un format : <uid>-<password>

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








