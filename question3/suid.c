#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
    int ruid = getuid();
    int rgid = getgid();
    int eiud = geteuid();
    int egid = getegid();

    printf("eiud: %d\n",eiud);
    printf("egid: %d\n",egid);
    printf("ruid: %d\n",ruid);
    printf("rgid: %d\n",rgid);

    FILE *f;
    if(argc < 2){
        printf("Missing Args");
        exit(EXIT_FAILURE);
    }
    f = fopen(argv[1], "r");
    if (f == NULL){
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }
    printf("File opens correctly\n");
    char ch;
    do
    {
        ch = fgetc(f);
        printf("%c", ch);
    } while (ch != EOF);
    fclose(f);    

    // TODO 
    return 0;
}
